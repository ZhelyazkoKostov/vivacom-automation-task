package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Utils;

public class AccessoriesPage {

    private WebDriver driver;
    private Utils utils;
    private HomePage homePage;
    private ShoppingPage shoppingPage;

    private By over40BGNSelectBox = By.xpath("//span[text()=\"над 40 лв.\"]/preceding-sibling::em");
    private By kosmosHeadPhonesLink = By.xpath("//h3[contains(text(),\"Слушалки Kosmos\")]/ancestor::a");
    private By showMoreButton = By.xpath("//button[text()=\"Покажи още\"]");

    public AccessoriesPage(WebDriver driver) {
        this.driver = driver;
        utils = new Utils();
        homePage = new HomePage(driver);
    }

    public ShoppingPage addHeadPhonesToShoppingCart() {
        homePage.waitElementPresent(over40BGNSelectBox, utils.getWaitForWaitMethod());
        homePage.clickElement(over40BGNSelectBox);
        homePage.waitElementPresent(showMoreButton, utils.getWaitForWaitMethod());
        homePage.clickElement(showMoreButton);
        homePage.waitElementPresent(kosmosHeadPhonesLink, utils.getWaitForWaitMethod());
        homePage.clickElement(kosmosHeadPhonesLink);
        shoppingPage = homePage.buyProduct();
        return new ShoppingPage(driver);

    }

}
