package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Utils;

public class ProductsPage {

    private WebDriver driver;
    private Utils utils;
    private HomePage homePage;

    private By huaweiManifacturerSelctBox = By.xpath("//span[text()=\"HUAWEI\"]/preceding-sibling::em");
    private By colorShowMore = By.xpath("//div[@id=\"filter-colors.color\"]/div[text()=\"Покажи още\"]");
    private By colorCrushBlue = By.xpath("//span[text()=\"CRUSH BLUE\"]");
    private By huaweiNova5T = By.xpath("//div[@class=\"list-compare-devices link-box-device-wrapper row\"]//a[1]");
    private By subscriptionRadioButton = By.xpath("//div[@id=\"related-offers-table-section-id\"]//span[contains(text(),\"579\")]");



    public ProductsPage(WebDriver driver){
        this.driver = driver;
        utils = new Utils();
        homePage = new HomePage(driver);
    }

    public void addPhoneToShoppingCart(){
        homePage.waitElementPresent(huaweiManifacturerSelctBox,utils.getWaitForWaitMethod());
        homePage.clickElement(huaweiManifacturerSelctBox);
        homePage.clickElement(colorShowMore);
        homePage.waitElementPresent(colorCrushBlue,utils.getWaitForWaitMethod());
        homePage.clickElement(colorCrushBlue);
        homePage.waitElementPresent(huaweiNova5T,utils.getWaitForWaitMethod());
        homePage.clickElement(huaweiNova5T);
        homePage.waitElementPresent(subscriptionRadioButton,utils.getWaitForWaitMethod());
        homePage.clickElement(subscriptionRadioButton);
        homePage.buyProduct();

    }






}
