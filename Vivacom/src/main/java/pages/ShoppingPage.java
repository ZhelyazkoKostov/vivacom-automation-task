package pages;

import com.google.common.base.Verify;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.Utils;

public class ShoppingPage {

    private WebDriver driver;
    private By continuePurchaseButton = By.xpath("//a[@title=\"Продължи с пазаруването\"]");
    private By totalAmountDue = By.xpath("//span[contains(text(),\"Обща сума\")]/following-sibling::span");
    private By removeItemXButton = By.xpath("//h3[contains(text(),\"Слушалки Kosmos\")]/ancestor::div[@class=\"product-wrapper \"]//em[@class=\"vivacom-icons icon-close_x\"]");
    private By cartItemsIcon = By.xpath("//span[@id=\"countAddedOffersToCart\" and text()=\"1\"]");
    private By creditRatePolicyAcceptCheckbox = By.xpath("//a[contains(text(),\"Политика за кредитна оценка\")]/preceding-sibling::label");
    private By inputCreditCheckTerms = By.xpath("//input[@id=\"internal-credit-check-terms-and-conditions\"]");
    private By privacyPolicyAcceptCheckbox = By.xpath("//a[contains(text(),\"Политика за поверителност\")]/preceding-sibling::label/em[@class=\"vivacom-icon icon-box_empty\"]");
    private By inputPrivacyCheckTerms = By.xpath("//input[@id=\"external-credit-check-terms-and-conditions\"]");
    private By orderCheckoutButtonInactive = By.xpath("//button[@class=\"btn btn-success js-checkout-btn disable-elm\"]");
    private By orderCheckoutButtonActive = By.xpath("//button[@class=\"btn btn-success js-checkout-btn\"]");
    private By popUpWindow = By.xpath("//div[@id=\"checkoutVoilationModal\"]//p[@class=\"modal-info-text nonLoggedModalGroup\"]");
    private By closePopWindowButton = By.xpath ("//div[@id=\"checkoutVoilationModal\"]//button[@class=\"btn-close modal-btn-close\"]");
    private By emptyBasketXButton = By.xpath("//button[@class=\"btn-close\"]/em[@class=\"vivacom-icons icon-close_x\"]");
    private By emptyBasketMessage = By.xpath("//h3[contains(text(),\"В момента кошницата ви е празна\")]");

    private HomePage homePage;
    private Utils utils;

    public ShoppingPage(WebDriver driver){
        this.driver = driver;
        homePage = new HomePage(driver);
        utils = new Utils();
    }


    public double getAmountDue(){
        double actualAmountDue;
        WebElement amountDue = driver.findElement(totalAmountDue);
        String stringPriece = amountDue.getText();
        String text = stringPriece.replace(" лв.","");
        System.out.println("Amount due: " + text);
        actualAmountDue= Double.parseDouble(text);
        return actualAmountDue;
    }

    public boolean verifyAmountDueDoesNotExceedLimit(double limit){
        boolean isLimitExceeded = false;
        double price = getAmountDue();
        if(limit < price){
            isLimitExceeded = true;
            System.out.println("Amount Due exceeds set Limit!");
        }
        return isLimitExceeded;
    }

    public void removeItem(String limitAmount){
        double limit = Double.parseDouble(limitAmount);
        boolean isLimitExceeded;

        isLimitExceeded = verifyAmountDueDoesNotExceedLimit(limit);
        if(isLimitExceeded){
            homePage.clickElement(removeItemXButton );
            System.out.println("Item removed from Shopping Cart!");
        }

    }

    public void isCreditRatePolicyAcceptCheckboxChecked(){

        boolean isChecked;
        homePage.waitElementPresent(cartItemsIcon,utils.getWaitForWaitMethod());
        isChecked = homePage. verifyCheckboxChecked(inputCreditCheckTerms);
        homePage.clickElement(creditRatePolicyAcceptCheckbox);
        isChecked = homePage. verifyCheckboxChecked(inputCreditCheckTerms);
        System.out.println("Credit Rate Policy Checkbox is in Click Status: " + isChecked);
        Verify.verify(isChecked);
    }
    public void isCheckoutOrderButtonActiveNeg(){
        boolean isActive;
        homePage.waitElementPresent(orderCheckoutButtonInactive,utils.getWaitForWaitMethod());
        isActive  = homePage. verifyElementActive(orderCheckoutButtonInactive);
        System.out.println("Checkout Order Button is not active: " + isActive);
        Verify.verify(isActive);
    }
    public void isPrivacyPolicyAcceptCheckboxChecked(){
        boolean isChecked;
        homePage.waitElementPresent(privacyPolicyAcceptCheckbox,utils.getWaitForWaitMethod());
        homePage.clickElement(privacyPolicyAcceptCheckbox);
        isChecked = homePage. verifyCheckboxChecked(inputPrivacyCheckTerms);
        System.out.println("Privacy Policy Checkbox is in Click Status: " + isChecked);
        Verify.verify(isChecked);
    }
    public void isCheckoutOrderButtonActivePos(){
        boolean isActive;
        isActive  = homePage. verifyElementActive(orderCheckoutButtonActive);
        System.out.println("Checkout Order Button is active: " + isActive);
        Verify.verify(isActive);
    }
    public void goToCheckOutOrder(){
        homePage.clickElement(orderCheckoutButtonActive);
    }
    public  void verifyPopUpWindowDisplayed(){
        boolean isDisplayed;
        goToCheckOutOrder();
        homePage.waitElementPresent(popUpWindow,utils.getWaitForWaitMethod());
        isDisplayed = homePage.verifyElementPresent(popUpWindow);
        if(isDisplayed){
            System.out.println("Login/Registration PopUp Displayed!");
        }
        Verify.verify(isDisplayed);
        homePage.clickElement(closePopWindowButton);
    }
    public void verifyEmptyBasketMessageDisplayed(){
        Boolean isDisplayed;
        homePage.waitElementPresent(emptyBasketXButton,utils.getWaitForWaitMethod());
        homePage.clickElement(emptyBasketXButton);
        homePage.waitElementPresent(emptyBasketMessage,utils.getWaitForWaitMethod());
        isDisplayed  = homePage.verifyElementPresent(emptyBasketMessage);
        if(isDisplayed){
            System.out.println("Shopping Cart is empty Message Displayed!");
        }
        Verify.verify(isDisplayed);
    }



}
