package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Utils;

public class HomePage {

    private WebDriver driver;
    private By logoVivacomHomePage = By.xpath("//a[@class=\"web-only navbar-brand logo\"]");
    private By devicesDropDown = By.xpath("//li[@class=\"dropdown\"]/a[text()=\"Устройства\"]");
    private By mobilePhonesMenu = By.xpath("//div[@class=\"dropdown-link-text\"]/a[contains(text(),\"Мобилни телефони\")]");
    private By myShoppingCartText = By.xpath("//h2[text()=\"Моята кошница\"]");
    private By accessoriesMenu = By.xpath("//a[contains(text(),\"Аксесоари\")]");
    private By buyButton = By.xpath("//span[@class=\"vivacom-icon icon-shopping_cart\"]/ancestor::button");
    private Utils utils;

    public HomePage(WebDriver driver){
        this.driver = driver;
        utils = new Utils();
    }


//    MAIN METHODS

    public void waitElementPresent(By element, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public void clickElement(By element){
        WebElement webElement = driver.findElement(element);
        webElement.click();
    }


    public boolean verifyElementPresent(By element, int seconds){
        boolean isElementPresent;
        waitElementPresent(element, seconds);
        WebElement webElement = driver.findElement(element);
        isElementPresent = webElement.isDisplayed();
        return isElementPresent;
    }

    public boolean verifyUserAtShoppingCartPage(){
        boolean isAtShoppingCart;
        isAtShoppingCart = verifyElementPresent(myShoppingCartText,utils.getWaitForWaitMethod());
        if(isAtShoppingCart){
            System.out.println("User is at Shopping Cart Page!");
        }
        return isAtShoppingCart;
    }

//    PODUCTS PAGE METHODS

    public ShoppingPage buyProduct(){
        waitElementPresent(buyButton,utils.getWaitForWaitMethod());
        clickElement(buyButton);
        System.out.println("New Product added to Shopping Cart!");
        return new ShoppingPage(driver);
    }

    public ProductsPage goToMobilePhones(){
        waitElementPresent(devicesDropDown, utils.getWaitForWaitMethod());
        clickElement(devicesDropDown);
        waitElementPresent(mobilePhonesMenu,utils.getWaitForWaitMethod());
        clickElement(mobilePhonesMenu);
        return new ProductsPage(driver);
    }

    public AccessoriesPage goToAccessories(){
        waitElementPresent(logoVivacomHomePage, utils.getWaitForWaitMethod());
        clickElement(logoVivacomHomePage);
        waitElementPresent(devicesDropDown, utils.getWaitForWaitMethod());
        clickElement(devicesDropDown);
        waitElementPresent(accessoriesMenu,utils.getWaitForWaitMethod());
        clickElement(accessoriesMenu);
        return new AccessoriesPage(driver);
    }

    //    SHOPPING CART PAGE METHODS

    public boolean verifyCheckboxChecked(By input){
        boolean isChecked;
        WebElement inputBox = driver.findElement(input);
        isChecked = inputBox.isSelected();
        return isChecked;
    }
    public boolean verifyElementActive(By element){
        boolean isActive;
        WebElement webElement = driver.findElement(element);
        isActive = webElement.isEnabled();
        return isActive;
    }
    public boolean verifyElementPresent(By element){
        boolean isVisible;
        WebElement webElement = driver.findElement(element);
        isVisible = webElement.isDisplayed();
        return isVisible;
    }


}
