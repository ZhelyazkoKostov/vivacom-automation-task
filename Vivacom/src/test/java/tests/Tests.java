package tests;

import base.BaseTests;
import com.google.common.base.Verify;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.AccessoriesPage;
import pages.ProductsPage;
import pages.ShoppingPage;

public class Tests extends BaseTests {

    private ProductsPage productsPage;
    private AccessoriesPage accessoriesPage;
    private ShoppingPage shoppingPage;
    private boolean isAtShoppingPage;

    @BeforeClass
    public void initPage(){
        productsPage = homePage.goToMobilePhones();

    }

    @Test (priority = 1)
    public void addPhoneToShoppingCart(){
        productsPage.addPhoneToShoppingCart();
        isAtShoppingPage = homePage.verifyUserAtShoppingCartPage();
        Verify.verify(isAtShoppingPage);

        accessoriesPage = homePage.goToAccessories();
        shoppingPage = accessoriesPage.addHeadPhonesToShoppingCart();
        isAtShoppingPage = homePage.verifyUserAtShoppingCartPage();
        Verify.verify(isAtShoppingPage);

        shoppingPage.removeItem(getConfigProperties("costLimit"));

        shoppingPage.isCreditRatePolicyAcceptCheckboxChecked ();
        shoppingPage.isCheckoutOrderButtonActiveNeg();
        shoppingPage.isPrivacyPolicyAcceptCheckboxChecked();
        shoppingPage.isCheckoutOrderButtonActivePos();
        shoppingPage.verifyPopUpWindowDisplayed();
        shoppingPage.verifyEmptyBasketMessageDisplayed();


    }

}
